package vip.justlive.frost.core.notify;

import org.redisson.Redisson;
import org.redisson.api.RScheduledExecutorService;
import vip.justlive.frost.core.config.JobConfig;
import vip.justlive.oxygen.core.ioc.Bean;
import vip.justlive.oxygen.core.ioc.Inject;

/**
 * redis实现的事件发布
 *
 * @author wubo
 */
@Bean
public class RedisEventPublisherImpl implements EventPublisher {

  private final RScheduledExecutorService executorService;

  @Inject
  public RedisEventPublisherImpl(Redisson redissonClient) {
    this.executorService = redissonClient.getExecutorService(JobConfig.EVENT);
  }

  @Override
  public void publish(Event event) {
    executorService.execute(new EventExecuteWrapper(event));
  }

}
